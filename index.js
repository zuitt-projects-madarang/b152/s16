/*Mini-Activity*/

function displayMsgToSelf(){
	console.log("Hello past self!");
}

displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();

/*While Loop*/

//allows us to repeat an action or an instruction as long as the condition is true.
/*
	1st loop - count 10
	2nd loop - count 9
	3rd loop - count 8
	4th loop - count 7
	5th loop - count 6
	6th loop - count 5
	7th loop - count 4
	8th loop - count 3
	9th loop - count 2
	10th loop - count 1

	while loop checked if count is still NOT equal to 0:
	At this point, before a possible 11th loop, count is decremented to 0.
	Therefore, there was no 11th loop.

	If there is no decrementation, the condition is always true, thus, an infinite loop.

	Infinite loops will run your code block forever until you stop it.

	An infinite loop is a piece of code that keeps running because a terminating condition is never reached.

	Always make sure that at the very least, your loop condition will be terminated or will be false at one point.
*/
	let count = 10;

	while (count !== 0){
		displayMsgToSelf();
		count--;
	}

	/*Mini-Activity (While-loop)*/

	let number = 1;
	while (number <= 15){
		console.log(number);
		number++;
	}

/*Do While Loop*/

	//Do While loop is similar to While loop. However, Do While loop will run the code block at least once before it checks the condition.

	//With While loop, we first check the condition and then run the code block/statements.

let doWhileCounter = 20;

do {
	console.log(doWhileCounter);
	--doWhileCounter;
} while (doWhileCounter > 0)

/*For Loop*/

	//A for loop is more flexible than while and do-while loops.

	//It consists of 3 parts:
		// 1. The "initialization" value that will track the progression of the loop
		// 2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
		// 3. The "finalExpression" indicates how to advance the loop.

	/*
		Syntax:

		for(initialization, condition, finalExpression){
			//codeblock / statement
		}
	*/

		//Create a loop that will start from 1 and end at 20


		for(let x = 1; x <= 20; x++){
			console.log(x);
		}

	//Q: Can we use for loop in a string? - Yes

		let name = "Nikko Handsome";

	//Accessing elements of a string
		//using index
		//index starts at 0

		console.log(name[0]); //N
		console.log(name[1]); //i
		console.log(name[2]); //k
		console.log(name[3]); //k
		console.log(name[4]); //o

	//Count of the string characters
		//using .length

		console.log(name.length); //5 characters

	//Use for loop to display each character of the string

		for(let index = 0; index < name.length; index++){
			console.log(name[index]);
		}

	//Using for loop in an array

		let fruits = ["mango", "apple", "orange", "banana", "strawberry", "kiwi"];
			//elements - each value inside the array
		console.log(fruits);

		//Total count of elements in an array
		console.log(fruits.length); //6 elements

		//Access each element in an array
		console.log(fruits[0]); //mango
		console.log(fruits[4]); //strawberry
		console.log(fruits[5]); //kiwi

		//How do we determine the last element in an array if we don't know the total number of the elements
		console.log(fruits[fruits.length - 1]);

		//if we want to assign new value to an element
		fruits[6] = "grapes";
		console.log(fruits);

	for(let i = 0; i < fruits.length; i++){
		console.log(fruits[i]);
	}

	//Example of Array of objects
	let cars = [
		{
			brand: "Toyota",
			type: "Sedan"
		},
		{
			brand: "Rollse Royce",
			type: "Luxury Sedan"
		},
		{
			brand: "Mazda",
			type: "Hatchback"
		}
	];

	console.log(cars.length); //3 elements

	console.log(cars[1]);

	/*Mini-Activity*/
	//Use for loop to display each element in the cars array

	for(let c = 0; c < cars.length; c++){
		console.log(cars[c]);
	}


	let myName = "Good Morning";
	let ind = 0;

	for (ind; ind < myName.length; ind++){
		if (myName[ind].toLowerCase() == "a" ||
			myName[ind].toLowerCase() == "e" ||
			myName[ind].toLowerCase() == "i" ||
			myName[ind].toLowerCase() == "o" ||
			myName[ind].toLowerCase() == "u"
			){
			console.log(3);
		} else {
			console.log(myName[ind]);
		}
	}

	//Continue and Break
		//The "continue" statement allows the code to go to the next iteration of the loop "without finishing the execution of the following statements in a code block".
		//Skips the current loop and proceeds to the next loop

		for (let a = 20; a > 0; a--){
			if(a % 2 === 0){
				continue;
			}

			if (a < 10){
				break;
			}
			console.log(a);
		}

	/*Mini-Activity*/

	for (let i = 0; i < myName.length; i++){
		if (myName[i].toLowerCase() === "a"){
			continue;
		}
		if (myName[i].toLowerCase() === "d"){
			break;
		}
		console.log(myName[i]);
	}

	//Q: Is it possible to have another loop inside a loop? - Yes
	//Nested Loops

		for(let x = 0; x <= 10; x++){
			console.log(x);
			for(let y = 0; y <= 10; y++){
				console.log(`${x} + ${y} = ${x+y}`)
			}
		}